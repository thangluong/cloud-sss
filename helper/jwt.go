package helper

import (
	"encoding/json"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
	"strings"
	"time"
)
import "github.com/dgrijalva/jwt-go"


func CreateToken(customerID primitive.ObjectID) (string, error) {
	claim := jwt.MapClaims{}
	claim["authorized"] = true
	claim["customer_id"] = customerID
	claim["exp"] = time.Now().Add(60* time.Minute).Unix()

	tokenUnsigned := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	tokenSigned, err := tokenUnsigned.SignedString([]byte("KEY"))
	if err != nil {
		log.Println(err)
		return "", err
	}
	return tokenSigned, nil
}

func ExtractTokenFromRequest(r *http.Request) string {
	bearerToken := r.Header.Get("Authorization")
	strArr := strings.Split(bearerToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}

func VerifyToken(r *http.Request) (string, error) {
	tokenString := ExtractTokenFromRequest(r)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _,ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			log.Printf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte("KEY"), nil
	})
	if err != nil {
		return "", err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims["customer_id"].(string), nil
	}
	return "", err
}

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		type Response struct {
			Message string `json:"message"`
			IsValid bool `json:"is_valid"`
		}
		customerID, err := VerifyToken(r)
		if err != nil {
			log.Println("Token is invalid")
			json.NewEncoder(w).Encode(&Response{
				Message: "Invalid token",
				IsValid: false,
			})
			return
		}
		q := r.URL.Query()
		q.Add("customer_id",customerID)
		r.URL.RawQuery = q.Encode()
		next.ServeHTTP(w, r)
	})
}

func GetCustomerIDLogged(request *http.Request) primitive.ObjectID {
	//log.Println(request.URL.Query().Get("customer_id"))
	customerID, _ := primitive.ObjectIDFromHex(request.URL.Query().Get("customer_id"))
	return customerID
}