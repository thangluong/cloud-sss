package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Shoe struct {
	ShoeId primitive.ObjectID `bson:"_id" json:"id"`
	Name string `bson:"name" json:"name"`
	Image string `bson:"image" json:"image"`
	Description string `bson:"description" json:"description"`
	CategoryId primitive.ObjectID `bson:"category_id" json:"-"`
}
