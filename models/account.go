package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Account struct {
	AccountId primitive.ObjectID `bson:"_id"`
	Phone string `bson:"phone"`
	Password string `bson:"password"`
}
