package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Customer struct {
	CustomerId primitive.ObjectID `bson:"_id" json:"id"`
	FirstName string `bson:"first_name" json:"first_name"`
	LastName string `bson:"last_name" json:"last_name"`
	Address string `bson:"address" json:"address"`
	AccountId primitive.ObjectID `bson:"account_id" json:"-"`
}