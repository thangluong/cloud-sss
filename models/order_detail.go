package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type OrderDetail struct {
	OrderDetailId primitive.ObjectID `bson:"_id" json:"-"`
	Quantity int `bson:"quantity" json:"quantity"`
	OrderId primitive.ObjectID `bson:"order_id" json:"-"`
	ShoeSizeId primitive.ObjectID `bson:"shoe_size_id" json:"shoe_size_id"`
}
