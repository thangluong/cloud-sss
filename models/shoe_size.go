package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type ShoeSize struct {
	ShoeSizeId primitive.ObjectID `bson:"_id" json:"id"`
	Price int `bson:"price" json:"price"`
	Available int `bson:"available" json:"available"`
	ShoeId primitive.ObjectID `bson:"shoe_id" json:"shoe_id"`
	SizeId primitive.ObjectID `bson:"size_id" json:"size_id"`
}
