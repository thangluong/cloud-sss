package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Size struct {
	SizeId primitive.ObjectID `bson:"_id" json:"id"'`
	Size int `bson:"size" json:"size"`
}
