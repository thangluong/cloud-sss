package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Order struct {
	OrderId primitive.ObjectID `bson:"_id" json:"order_id"`
	OrderDate string `bson:"order_date" json:"order_date"`
	Total int `bson:"total" json:"total"`
	CustomerId primitive.ObjectID `bson:"customer_id" json:"-"`
}