module sssapi

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/rs/cors v1.7.0 // indirect
	go.mongodb.org/mongo-driver v1.3.4
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
)
