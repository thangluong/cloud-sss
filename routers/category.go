package routers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"log"
	"net/http"
	"sssapi/api"
	"sssapi/models"
)

func CategoryRoute(r chi.Router) {
	r.Get("/", getAllCategory)
	r.Get("/{categoryID}", getCategoryDetailsByCategoryID)
	r.Post("/", createNewCategory)
}

func getAllCategory(w http.ResponseWriter, r *http.Request) {
	type Response struct {
		Categories []models.Category `json:"categories"`
	}
	categories := api.GetAllCategory()
	response := Response{
		Categories: categories,
	}
	json.NewEncoder(w).Encode(response)
}

func getCategoryDetailsByCategoryID(w http.ResponseWriter, r *http.Request) {
	categoryID := chi.URLParam(r, "categoryID")

	type ResponseData struct {
		CategoryName    string               `json:"category_name"`
		Shoes []models.Shoe `json:"shoes"`
	}
	type Response struct {
		Message string `json:"message"`
		IsSuccess bool `json:"is_success"`
		Data *ResponseData `json:"data,omitempty"`
	}
	response := Response{
		Message: "Get category details failed",
		IsSuccess: false,
		Data: nil,
	}
	responseData := ResponseData{}
	if isSuccess, category := api.GetCategoryByCategoryID(categoryID); isSuccess {
		responseData.CategoryName = category.Name
		responseData.Shoes = []models.Shoe{}
		if isSuccess, shoes := api.GetShoesByCategoryID(categoryID); isSuccess {
			responseData.Shoes = shoes

			response.Message = "Get category details successfully"
			response.IsSuccess = true
			response.Data = &responseData
		}
	}

	json.NewEncoder(w).Encode(response)
}

func createNewCategory(w http.ResponseWriter, r *http.Request) {
	type RequestBody struct {
		Name string `json:"name"`
	}
	type Response struct {
		Message   string           `json:"message"`
		IsSuccess bool             `json:"is_success"`
		Data      *models.Category `json:"data"`
	}
	body := RequestBody{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		log.Println(err)
	}
	response := Response{
		Message:   "Creating new category failed",
		IsSuccess: false,
	}
	isSuccess, category := api.CreateNewCategory(body.Name)
	if isSuccess {
		response.Message = "Create new category successfully"
		response.IsSuccess = true
		response.Data = category
	}
	if !isSuccess && category != nil {
		response.Message = "Category already existed"
		response.Data = category
	}
	json.NewEncoder(w).Encode(response)
}
