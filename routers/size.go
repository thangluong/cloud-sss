package routers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"log"
	"net/http"
	"sssapi/api"
	"sssapi/helper"
	"sssapi/models"
)

func SizeRoute(r chi.Router) {
	r.Get("/", getAllSize)
	r.With(helper.AuthMiddleware).Post("/", createNewSize)
}

func getAllSize(writer http.ResponseWriter, request *http.Request)  {
	type Response struct {
		Sizes []models.Size `json:"sizes"`
	}
	sizes := api.GetAllSize()
	response := Response{
		Sizes: sizes,
	}
	json.NewEncoder(writer).Encode(response)
}

func createNewSize(writer http.ResponseWriter, request *http.Request) {
	type RequestBody struct {
		Size int `json:"size"`
	}
	type Response struct {
		Message   string       `json:"message"`
		IsSuccess bool         `json:"is_success"`
		Data      *models.Size `json:"data,omitempty"`
	}
	body := RequestBody{}
	if err := json.NewDecoder(request.Body).Decode(&body); err != nil {
		log.Println("Cannot decode SIZE request body")
	}
	response := Response{
		Message:   "Adding new size failed",
		IsSuccess: false,
	}
	isSuccess, size := api.CreateNewSize(body.Size)
	if isSuccess {
		response.Message = "Add new size successfully"
		response.IsSuccess = true
		response.Data = size
	}
	if !isSuccess && size != nil {
		response.Message = "Size already existed"
		response.Data = size
	}
	json.NewEncoder(writer).Encode(response)
}
