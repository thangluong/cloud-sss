package routers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
	"sssapi/api"
	"sssapi/helper"
	"sssapi/models"
	"time"
)

func OrderRoute(r chi.Router) {
	r.With(helper.AuthMiddleware).Post("/", createOrder)
	r.Get("/{orderID}", getOrderByOrderID)
}

func getOrderByOrderID(w http.ResponseWriter, r *http.Request) {
	orderID := chi.URLParam(r, "orderID")

	//type RequestBody struct {
	//
	//}
	//body := RequestBody{}
	//if err := json.NewDecoder(r.).Decode(&body); err != nil {
	//	log.Println(err)
	//}
	//body
	type ResponseData struct {
		OrderID      primitive.ObjectID   `json:"order_id"`
		OrderDate    string               `json:"order_date"`
		Total        int                  `json:"total"`
		OrderDetails []models.OrderDetail `json:"order_details"`
	}
	type Response struct {
		Message string `json:"message"`
		IsSuccess bool `json:"is_success"`
		Data *ResponseData `json:"data,omitempty"`
	}
	response := Response{
		Message: "Get order details failed",
		IsSuccess: false,
		Data: nil,
	}
	responseData := ResponseData{}
	if isSuccess, order := api.GetOrderByOrderID(orderID); isSuccess {
		responseData.OrderID = order.OrderId
		responseData.OrderDate = order.OrderDate
		responseData.Total = order.Total
		responseData.OrderDetails = []models.OrderDetail{}
		if isSuccess, orderDetails := api.GetOrderDetailsByOrderID(orderID); isSuccess {
			responseData.OrderDetails = orderDetails

			response.Message = "Get order details successfully"
			response.IsSuccess = true
			response.Data = &responseData
		}
	}

	json.NewEncoder(w).Encode(response)
}

func createOrder(write http.ResponseWriter, request *http.Request) {
	//input format: [
	//{ shoe_size_id, quantity }
	//...
	//]
	//type RequestBody struct {
	//	ShoeSizeId primitive.ObjectID `json:"shoe_size_id"`
	//	Quantity   int                `json:"quantity"`
	//}
	type Response struct {
		Message   string        `json:"message"`
		IsSuccess bool          `json:"is_ok"`
		Data      *models.Order `json:"data,omitempty"`
	}
	body := make([]api.RequestUpdateShoeSizesBody, 0)
	response := Response{
		Message:   "Create new order failed",
		IsSuccess: false,
	}
	if err := json.NewDecoder(request.Body).Decode(&body); err != nil {
		log.Println(err)
	} else {
		idArr := make([]primitive.ObjectID, 0)
		for _, item := range body {
			idArr = append(idArr, item.ShoeSizeId)
		}
		shoeSizes := api.FindMultiShoeSize(idArr)
		totalPrice := 0
		outOfStock := false
		for i, b := range body {
			for _, obj := range shoeSizes {
				if b.ShoeSizeId == obj.ShoeSizeId {
					totalPrice += obj.Price * body[i].Quantity
					if b.Quantity > obj.Available {
						outOfStock = true
					}
				}
			}
		}
		if !outOfStock {
			now := time.Now().Format(http.TimeFormat)
			customerID := helper.GetCustomerIDLogged(request)
			if isSuccess, order := api.CreateNewOrder(now, totalPrice, customerID); isSuccess {
				orderDetails := make([]models.OrderDetail, 0)
				for _, item := range body {
					orderDetails = append(orderDetails, models.OrderDetail{
						OrderDetailId: primitive.NewObjectID(),
						Quantity:      item.Quantity,
						OrderId:       order.OrderId,
						ShoeSizeId:    item.ShoeSizeId,
					})
				}
				isODSuccess := api.CreateNewOrderDetail(orderDetails)
				updatedAvailable := api.UpdateAvailableShoeSize(body)
				if isODSuccess && updatedAvailable {
					response.Message = "Create new order successfully"
					response.IsSuccess = true
					response.Data = order
				}
			}
		} else {
			response.Message = "Some shoes are out of stock"
		}
	}
	json.NewEncoder(write).Encode(response)
}
