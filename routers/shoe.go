package routers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
	"sssapi/api"
	"sssapi/models"
)

func ShoeRoute(r chi.Router) {
	r.Get("/", getAllShoe)
	r.Get("/{shoeID}", getShoeDetailsByShoeID)
	r.Post("/", createNewShoe)
}

func getAllShoe(w http.ResponseWriter, r *http.Request) {
	type Response struct {
		Shoes []models.Shoe `json:"shoes"`
	}
	shoes := api.GetAllShoe()
	response := Response{
		Shoes: shoes,
	}
	json.NewEncoder(w).Encode(response)
}

func getShoeDetailsByShoeID(w http.ResponseWriter, r *http.Request) {
	shoeID := chi.URLParam(r, "shoeID")

	type ShoeSize struct {
		ShoeSizeID primitive.ObjectID `json:"id"`
		Price      int                `json:"price"`
		Available  int                `json:"available"`
		Size       int                `json:"size"`
	}
	type ResponseData struct {
		ShoeName    string     `json:"shoe_name"`
		Description string     `json:"description"`
		Image string `json:"image"`
		ShoeSizes   []ShoeSize `json:"shoe_sizes"`
	}
	type Response struct {
		Message   string        `json:"message"`
		IsSuccess bool          `json:"is_success"`
		Data      *ResponseData `json:"data,omitempty"`
	}
	response := Response{
		Message:   "Get shoe details failed",
		IsSuccess: false,
		Data:      nil,
	}
	responseData := ResponseData{}
	if isSuccess, shoe := api.GetShoeByShoeID(shoeID); isSuccess {
		responseData.ShoeName = shoe.Name
		responseData.Description = shoe.Description
		responseData.Image = shoe.Image
		responseData.ShoeSizes = []ShoeSize{}
		if isSuccess, shoeSizes := api.GetShoeSizesByShoeID(shoeID); isSuccess {
			for _, shoeSize := range shoeSizes {
				if isSuccess, size := api.GetSizeBySizeID(shoeSize.SizeId.Hex()); isSuccess {
					responseData.ShoeSizes = append(responseData.ShoeSizes, ShoeSize{
						ShoeSizeID: shoeSize.ShoeSizeId,
						Price:      shoeSize.Price,
						Available:  shoeSize.Available,
						Size:       size.Size,
					})
				}
			}
			response.Message = "Get shoe details successfully"
			response.IsSuccess = true
			response.Data = &responseData
		}
	}

	json.NewEncoder(w).Encode(response)
}

func createNewShoe(w http.ResponseWriter, r *http.Request) {
	type RequestBody struct {
		Name        string             `json:"name"`
		Description string             `json:"description"`
		Image string `json:"image"`
		CategoryID  primitive.ObjectID `json:"category_id"`
	}
	type Response struct {
		Message   string       `json:"message"`
		IsSuccess bool         `json:"is_success"`
		Data      *models.Shoe `json:"data,omitempty"`
	}
	body := RequestBody{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		log.Println(err)
	}
	response := Response{
		Message:   "Creating new shoe failed",
		IsSuccess: false,
	}
	isSuccess, shoe := api.CreateNewShoe(body.Name, body.Description, body.Image, body.CategoryID)
	if isSuccess {
		response.Message = "Create new shoe successfully"
		response.IsSuccess = true
		response.Data = shoe
	}
	if !isSuccess && shoe != nil {
		response.Message = "Shoe already existed"
		response.Data = shoe
	}
	json.NewEncoder(w).Encode(response)
}
