package routers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"log"
	"net/http"
	"sssapi/api"
)

func AuthRoute(r chi.Router)  {
	r.Post("/login", login)
	r.Post("/register", register)
}

func login(writer http.ResponseWriter, request *http.Request)  {
	type RequestBody struct {
		Phone string `json:"phone"`
		Password string `json:"password"`
	}
	type Response struct {
		Message string `json:"message"`
		Token string `json:"token,omitempty"`
		Customer *api.User `json:"customer,omitempty"`
		IsOk bool `json:"is_ok"`
	}
	response := Response{}
	body := RequestBody{}
	if err := json.NewDecoder(request.Body).Decode(&body); err != nil {
		log.Println(err)
	}
	isValid, token, userData := api.Login(body.Phone, body.Password)
	if isValid {
		response = Response{Message: "Login success!", Token: token, Customer: userData, IsOk: true}
	} else {
		response = Response{Message: "Invalid username or password!", IsOk: false}
	}
	json.NewEncoder(writer).Encode(response)
}

func register(writer http.ResponseWriter, request *http.Request)  {
	type RequestBody struct {
		FirstName string `json:"first_name"`
		LastName string `json:"last_name"`
		Address string `json:"address"`
		Phone string `json:"phone"`
		Password string `json:"password"`
	}
	type Response struct {
		Message string `json:"message"`
		IsSuccess bool `json:"is_success"`
	}
	response := Response{}
	body := RequestBody{}
	if err:= json.NewDecoder(request.Body).Decode(&body); err!= nil {
		log.Println(err)
	}
	if isSuccess := api.Register(body.FirstName, body.LastName, body.Address, body.Phone, body.Password); isSuccess {
		response = Response{
			Message: "Register successfully!",
			IsSuccess: true,
		}
	} else {
		response = Response{
			Message: "Register fail!",
			IsSuccess: false,
		}
	}
	json.NewEncoder(writer).Encode(response)
}