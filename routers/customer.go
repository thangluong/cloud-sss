package routers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"net/http"
	"sssapi/api"
	"sssapi/models"
)

func CustomerRoute(r chi.Router)  {
	r.Get("/", viewCustomer)
}

func viewCustomer(writer http.ResponseWriter, request *http.Request)  {
	type CustomerMd struct {
		FullName string `json:"full_name"`
		Address string `json:"address"`
	}
	type Response struct {
		Customers []models.Customer `json:"customers"`
	}
	var response Response
	customers := api.ViewCustomer()
	var list []CustomerMd
	for _,v := range customers {
		list = append(list, CustomerMd{FullName: v.FirstName + " " + v.LastName, Address: v.Address} )
	}
	response = Response{Customers: customers}
	json.NewEncoder(writer).Encode(response)
}