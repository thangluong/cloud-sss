package routers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
	"sssapi/api"
	"sssapi/models"
)

func ShoeSizeRoute(r chi.Router)  {
	r.Get("/", getAllShoeSize)
	r.Post("/", createNewShoeSize)
}

func getAllShoeSize(w http.ResponseWriter, r *http.Request)  {
	type Response struct {
		ShoeSizes []models.ShoeSize `json:"shoe_sizes"`
	}
	shoeSizes := api.GetAllShoeSizes()
	response := Response{
		ShoeSizes: shoeSizes,
	}
	json.NewEncoder(w).Encode(response)
}

func createNewShoeSize(w http.ResponseWriter, r *http.Request)  {
	type RequestBody struct {
		Price int `json:"price"`
		Available int `json:"available"`
		ShoeID primitive.ObjectID `json:"shoe_id"`
		SizeID primitive.ObjectID `json:"size_id"`
	}
	type Response struct {
		Message string `json:"message"`
		IsSuccess bool `json:"is_success"`
		Data *models.ShoeSize `json:"data,omitempty"`
	}
	body := RequestBody{}
	if err := json.NewDecoder(r.Body).Decode(&body);err!=nil {
		log.Println(err)
	}
	response := Response{
		Message: "Create new shoe size failed",
		IsSuccess: false,
	}
	isSuccess, shoeSize := api.CreateNewShoeSize(body.Price, body.Available, body.ShoeID, body.SizeID)
	if isSuccess {
		response.Message = "Create new shoe size successfully"
		response.IsSuccess = true
		response.Data = shoeSize
	}
	if !isSuccess && shoeSize != nil {
		response.Message = "Shoe size already existed"
		response.Data = shoeSize
	}
	json.NewEncoder(w).Encode(response)
}