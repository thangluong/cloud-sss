package main

import (
	"github.com/go-chi/chi"
	"github.com/rs/cors"
	"net/http"
	"sssapi/routers"
)

func Handler(options cors.Options) func(next http.Handler) http.Handler {
	c := cors.New(options)
	return c.Handler
}

func main() {
	r := chi.NewRouter()
	r.Use(Handler(cors.Options{
		AllowedOrigins: []string{"*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300,
	}))
	r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
		writer.Write([]byte("Welcome!"))
	})

	r.Route("/auth", routers.AuthRoute)
	r.Route("/customers", routers.CustomerRoute)
	r.Route("/orders", routers.OrderRoute)
	r.Route("/sizes", routers.SizeRoute)
	r.Route("/shoes", routers.ShoeRoute)
	r.Route("/categories", routers.CategoryRoute)
	r.Route("/shoe_sizes", routers.ShoeSizeRoute)

	http.ListenAndServe(":2101",r)
}
