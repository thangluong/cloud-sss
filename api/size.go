package api

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"sssapi/constant"
	"sssapi/database"
	"sssapi/models"
	"time"
)

type Size models.Size

func GetAllSize() []models.Size {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	sizes := make([]models.Size, 0)
	col := database.GetConnection().Collection(constant.SIZE_TABLE)
	if res, err := col.Find(ctx, bson.M{}); err != nil {
		log.Println(err)
	} else {
		for res.Next(ctx) {
			var size models.Size
			if err := res.Decode(&size); err != nil {
				log.Println(err)
			} else {
				sizes = append(sizes, size)
			}
		}
	}
	return sizes
}

func GetSizeBySizeID(sizeID string) (bool, *models.Size) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	size := models.Size{}
	colA := database.GetConnection().Collection(constant.SIZE_TABLE)
	objID, _ := primitive.ObjectIDFromHex(sizeID)
	if err := colA.FindOne(ctx, bson.M{"_id": objID}).Decode(&size); err != nil {
		log.Println(err)
		return false, nil
	}
	return true, &size
}

func CreateNewSize(newSize int) (bool, *models.Size) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if newSize < 1 {
		return false, nil
	}
	size := Size{
		SizeId: primitive.NewObjectID(),
		Size:   newSize,
	}
	col := database.GetConnection().Collection(constant.SIZE_TABLE)
	if err := col.FindOne(ctx, bson.M{"size": newSize}).Decode(&size); err != nil {
		if _, err := col.InsertOne(ctx, &size); err == nil {
			return true, (*models.Size)(&size)
		}
	}
	return false, (*models.Size)(&size)
}
