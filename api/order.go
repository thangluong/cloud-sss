package api

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"sssapi/constant"
	"sssapi/database"
	"sssapi/models"
	"time"
)

type Order models.Order

func GetOrderByOrderID(orderID string) (bool, *models.Order) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	order := models.Order{}
	colA := database.GetConnection().Collection(constant.ORDER_TABLE)
	objID, _ := primitive.ObjectIDFromHex(orderID)
	if err := colA.FindOne(ctx, bson.M{"_id": objID}).Decode(&order); err != nil {
		log.Println(err)
		return false, nil
	}
	return true, &order
}

func CreateNewOrder(orderDate string, total int, customerID primitive.ObjectID) (bool, *models.Order) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	order := Order{
		OrderId:    primitive.NewObjectID(),
		OrderDate:  orderDate,
		Total:      total,
		CustomerId: customerID,
	}
	col := database.GetConnection().Collection(constant.ORDER_TABLE)

	if _, err := col.InsertOne(ctx, &order); err != nil {
		return false, nil
	} else {
		return true, (*models.Order)(&order)
	}
}
