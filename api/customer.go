package api

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"sssapi/constant"
	"sssapi/database"
	"sssapi/models"
	"time"
)

type Customer models.Customer

func GetCustomerIdByPhone(phone string) (bool, primitive.ObjectID) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	account := Account{}
	colA := database.GetConnection().Collection(constant.ACCOUNT_TABLE)
	if err := colA.FindOne(ctx, bson.M{"phone": phone}).Decode(&account); err != nil {
		log.Println(err)
		return false, primitive.ObjectID{}
	}
	customer := Customer{}
	colC := database.GetConnection().Collection(constant.CUSTOMER_TABLE)
	if err := colC.FindOne(ctx, bson.M{"account_id": account.AccountId}).Decode(&customer); err != nil {
		log.Println(err)
		return false, primitive.ObjectID{}
	}
	return true, customer.CustomerId
}

func ViewCustomer() []models.Customer  {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var customers []models.Customer
	col := database.GetConnection().Collection(constant.CUSTOMER_TABLE)
	if res, err := col.Find(ctx, bson.M{}); err != nil {
		log.Println(err)
	} else {
		for res.Next(ctx) {
			var customer models.Customer
			if err := res.Decode(&customer); err != nil {
				log.Println(err)
			} else {
				customers = append(customers, customer)
			}
		}
	}
	return customers
}
