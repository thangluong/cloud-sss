package api

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"sssapi/constant"
	"sssapi/database"
	"sssapi/models"
	"time"
)

type Shoe models.Shoe

func GetAllShoe() []models.Shoe {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	shoes := make([]models.Shoe, 0)
	col := database.GetConnection().Collection(constant.SHOE_TABLE)
	if res, err := col.Find(ctx, bson.M{}); err != nil {
		log.Println(err)
	} else {
		for res.Next(ctx) {
			var shoe models.Shoe
			if err := res.Decode(&shoe); err != nil {
				log.Println(err)
			} else {
				shoes = append(shoes, shoe)
			}
		}
	}
	return shoes
}

func GetShoeByShoeID(shoeID string) (bool, *models.Shoe) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	shoe := models.Shoe{}
	colA := database.GetConnection().Collection(constant.SHOE_TABLE)
	objID, _ := primitive.ObjectIDFromHex(shoeID)
	if err := colA.FindOne(ctx, bson.M{"_id": objID}).Decode(&shoe); err != nil {
		log.Println(err)
		return false, nil
	}
	return true, &shoe
}

func GetShoesByCategoryID(categoryID string) (bool, []models.Shoe) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	shoes := make([]models.Shoe,0)
	colA := database.GetConnection().Collection(constant.SHOE_TABLE)
	objID, _ := primitive.ObjectIDFromHex(categoryID)
	if res, err := colA.Find(ctx, bson.M{"category_id": objID}); err != nil {
		log.Println(err)
		return false, nil
	} else {
		var shoe models.Shoe
		for res.Next(ctx) {
			if err := res.Decode(&shoe); err != nil {
				log.Println(err)
			} else {
				shoes = append(shoes, shoe)
			}
		}
	}
	return true, shoes
}

func CreateNewShoe(name string, description string, image string, categoryID primitive.ObjectID) (bool, *models.Shoe) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	category := models.Category{}
	colC := database.GetConnection().Collection(constant.CATEGORY_TABLE)
	if err := colC.FindOne(ctx, bson.M{"_id": categoryID}).Decode(&category); err != nil {
		return false, nil
	} else {
		shoe := Shoe{
			ShoeId:      primitive.NewObjectID(),
			Name: name,
			Description: description,
			Image: image,
			CategoryId:  category.CategoryId,
		}
		colS := database.GetConnection().Collection(constant.SHOE_TABLE)
		if err := colS.FindOne(ctx, bson.M{"name": name}).Decode(&shoe); err != nil {
			if _, err := colS.InsertOne(ctx, &shoe); err == nil {
				return true, (*models.Shoe)(&shoe)
			}
		}
		return false, (*models.Shoe)(&shoe)
	}
}
