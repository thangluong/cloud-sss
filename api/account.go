package api

import (
	"context"
	"log"
	"sssapi/constant"
	"sssapi/database"
	"sssapi/helper"
	"sssapi/models"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Account models.Account

type User struct {
	CustomerID string `json:"id"`
	FullName string `json:"full_name"`
}

func Login(phone string, password string) (bool, string, *User) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	account := Account{}
	colA := database.GetConnection().Collection(constant.ACCOUNT_TABLE)
	if err := colA.FindOne(ctx, bson.M{"phone": phone}).Decode(&account); err != nil {
		log.Println(err)
	}
	if helper.ComparePassword(password, account.Password) {
		customer := Customer{}
		colC := database.GetConnection().Collection(constant.CUSTOMER_TABLE)
		if err := colC.FindOne(ctx, bson.M{"account_id": account.AccountId}).Decode(&customer); err != nil {
			log.Println(err)
		}
		if token, err := helper.CreateToken(customer.CustomerId); err == nil {
			return true, token, &User{CustomerID: customer.CustomerId.Hex(), FullName: customer.FirstName + " " + customer.LastName}
		}
	}
	return false, "", nil
}

// Register write for customer.
func Register(firstName string, lastName string, address string, phone string, password string) bool {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	defer cancel()

	account := Account{
		AccountId: primitive.NewObjectID(),
		Phone:     phone,
		Password:  helper.HashPassword(password),
	}

	colA := database.GetConnection().Collection(constant.ACCOUNT_TABLE)

	_, err := database.GetConnection().Collection(constant.ACCOUNT_TABLE).Indexes().CreateOne(context.Background(), mongo.IndexModel{
		Keys:    bson.D{{"phone", 1}},
		Options: options.Index().SetUnique(true),
	})

	if err != nil {
		log.Println(err)
		return false
	}

	res, err := colA.InsertOne(ctx, &account)
	if err != nil {
		log.Println("Phone existed!")
		return false
	}

	customer := Customer{
		CustomerId: primitive.NewObjectID(),
		FirstName:  firstName,
		LastName:   lastName,
		Address:    address,
		AccountId:  res.InsertedID.(primitive.ObjectID),
	}

	colC := database.GetConnection().Collection(constant.CUSTOMER_TABLE)

	_, err1 := database.GetConnection().Collection(constant.CUSTOMER_TABLE).Indexes().CreateOne(context.Background(), mongo.IndexModel{
		Keys:    bson.D{{"account_id", 1}},
		Options: options.Index().SetUnique(true),
	})

	if err1 != nil {
		log.Println(err1)
		return false
	}

	res1, err1 := colC.InsertOne(ctx, &customer)
	if err1 != nil {
		log.Println("Customer existed!")
		return false
	}
	log.Printf("New account created with id: %s, customer id: %s", res.InsertedID.(primitive.ObjectID).Hex(), res1.InsertedID.(primitive.ObjectID).Hex())

	return true
}
