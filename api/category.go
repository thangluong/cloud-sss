package api

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"sssapi/constant"
	"sssapi/database"
	"sssapi/models"
	"time"
)
type Category models.Category

func GetAllCategory() []models.Category {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	categories := make([]models.Category, 0)
	col := database.GetConnection().Collection(constant.CATEGORY_TABLE)
	if res, err := col.Find(ctx, bson.M{}); err != nil {
		log.Println(err)
	} else {
		for res.Next(ctx) {
			var category models.Category
			if err := res.Decode(&category); err != nil {
				log.Println(err)
			} else {
				categories = append(categories, category)
			}
		}
	}
	return categories
}

func GetCategoryByCategoryID(categoryID string) (bool, *models.Category) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	category := models.Category{}
	colA := database.GetConnection().Collection(constant.CATEGORY_TABLE)
	objID, _ := primitive.ObjectIDFromHex(categoryID)
	if err := colA.FindOne(ctx, bson.M{"_id": objID}).Decode(&category); err != nil {
		log.Println(err)
		return false, nil
	}
	return true, &category
}

func CreateNewCategory(name string) (bool,*models.Category) {
	ctx,cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	category := Category{
		CategoryId: primitive.NewObjectID(),
		Name: name,
	}
	col := database.GetConnection().Collection(constant.CATEGORY_TABLE)
	if err:= col.FindOne(ctx, bson.M{"name": name}).Decode(&category); err!=nil {
		if _, err := col.InsertOne(ctx, &category); err == nil {
			return true, (*models.Category)(&category)
		}
	}
	return false, (*models.Category)(&category)
}
