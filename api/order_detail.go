package api

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"log"
	"sssapi/constant"
	"sssapi/database"
	"sssapi/models"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type OrderDetail models.OrderDetail

func CreateNewOrderDetail(orderDetails []models.OrderDetail) bool {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	col := database.GetConnection().Collection(constant.ORDER_DETAIL_TABLE)
	var itf []interface{}
	for _, od := range orderDetails {
		itf = append(itf, od)
	}
	if _, err := col.InsertMany(ctx, itf); err != nil {
		log.Println(err)
		return false
	}
	return true
}

func GetOrderDetailsByOrderID(orderID string) (bool, []models.OrderDetail) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	orderDetails := make([]models.OrderDetail,0)
	colA := database.GetConnection().Collection(constant.ORDER_DETAIL_TABLE)
	objID, _ := primitive.ObjectIDFromHex(orderID)
	if res, err := colA.Find(ctx, bson.M{"order_id": objID}); err != nil {
		log.Println(err)
		return false, nil
	} else {
		for res.Next(ctx) {
			var orderDetail models.OrderDetail
			if err := res.Decode(&orderDetail); err != nil {
				log.Println(err)
			} else {
				orderDetails = append(orderDetails, orderDetail)
			}
		}
	}
	return true, orderDetails
}

func CreateOrderDetail(orderDetails []OrderDetail) bool {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	defer cancel()

	col := database.GetConnection().Collection(constant.ORDER_DETAIL_TABLE)
	var session mongo.Session
	var err error

	if session, err = database.GetClient().StartSession(); err != nil {
		log.Println(err)
	}
	if err = session.StartTransaction(); err != nil {
		log.Println(err)
	}
	if err = mongo.WithSession(ctx, session, func(sessionContext mongo.SessionContext) error {
		for _, v := range orderDetails {
			orderDetail := OrderDetail{
				OrderDetailId: primitive.NewObjectID(),
				Quantity:      v.Quantity,
				OrderId:       v.OrderId,
				ShoeSizeId:    v.ShoeSizeId,
			}
			if _, insertErr := col.InsertOne(ctx, &orderDetail); insertErr != nil {
				sessionContext.AbortTransaction(sessionContext)
			}
		}
		sessionContext.CommitTransaction(sessionContext)
		return nil
	}); err != nil {
		log.Println(err)
	}
	session.EndSession(ctx)
	return true
}
