package api

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"sssapi/constant"
	"sssapi/database"
	"sssapi/models"
	"time"
)

type ShoeSize models.ShoeSize

func FindMultiShoeSize(shoeSizeIDs []primitive.ObjectID) []models.ShoeSize {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	shoeSize := models.ShoeSize{}
	shoeSizes := make([]models.ShoeSize, 0)
	col := database.GetConnection().Collection(constant.SHOE_SIZE_TABLE)
	if res, err := col.Find(ctx, bson.M{"_id": bson.M{"$in": shoeSizeIDs}}); err == nil {
		for res.Next(ctx) {
			if err := res.Decode(&shoeSize); err != nil {
				log.Println(err)
			} else {
				shoeSizes = append(shoeSizes, shoeSize)
			}
		}
	}
	return shoeSizes
}

func GetAllShoeSizes() []models.ShoeSize {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	shoeSizes := make([]models.ShoeSize, 0)
	col := database.GetConnection().Collection(constant.SHOE_SIZE_TABLE)
	if res, err := col.Find(ctx, bson.M{}); err != nil {
		log.Println(err)
	} else {
		var shoeSize models.ShoeSize
		for res.Next(ctx) {
			if err := res.Decode(&shoeSize); err != nil {
				log.Println(err)
			} else {
				shoeSizes = append(shoeSizes, shoeSize)
			}
		}
	}
	return shoeSizes
}

func GetShoeSizesByShoeID(shoeID string) (bool, []models.ShoeSize) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	shoeSizes := make([]models.ShoeSize,0)
	colA := database.GetConnection().Collection(constant.SHOE_SIZE_TABLE)
	objID, _ := primitive.ObjectIDFromHex(shoeID)
	if res, err := colA.Find(ctx, bson.M{"shoe_id": objID}); err != nil {
		log.Println(err)
		return false, nil
	} else {
		for res.Next(ctx) {
			var shoeSize models.ShoeSize
			if err := res.Decode(&shoeSize); err != nil {
				log.Println(err)
			} else {
				shoeSizes = append(shoeSizes, shoeSize)
			}
		}
	}
	return true, shoeSizes
}

func CreateNewShoeSize(price int, available int, shoeID primitive.ObjectID, sizeID primitive.ObjectID) (bool, *models.ShoeSize) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	shoe := models.Shoe{}
	size := models.Size{}
	colShoe := database.GetConnection().Collection(constant.SHOE_TABLE)
	colSize := database.GetConnection().Collection(constant.SIZE_TABLE)
	errShoe := colShoe.FindOne(ctx, bson.M{"_id": shoeID}).Decode(&shoe)
	errSize := colSize.FindOne(ctx, bson.M{"_id": sizeID}).Decode(&size)
	if errShoe != nil || errSize != nil {
		return false, nil
	} else {
		shoeSize := ShoeSize{
			ShoeSizeId: primitive.NewObjectID(),
			Price:      price,
			Available:  available,
			ShoeId:     shoeID,
			SizeId:     sizeID,
		}
		colShoeSize := database.GetConnection().Collection(constant.SHOE_SIZE_TABLE)
		if err := colShoeSize.FindOne(ctx, bson.M{"shoe_id": shoeID, "size_id": sizeID}).Decode(&shoeSize); err != nil {
			if _, err := colShoeSize.InsertOne(ctx, &shoeSize); err == nil {
				return true, (*models.ShoeSize)(&shoeSize)
			}
		}
		return false, (*models.ShoeSize)(&shoeSize)
	}
}

type RequestUpdateShoeSizesBody struct {
	ShoeSizeId primitive.ObjectID `json:"shoe_size_id"`
	Quantity   int                `json:"quantity"`
}

func UpdateAvailableShoeSize(body []RequestUpdateShoeSizesBody) bool {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	col := database.GetConnection().Collection(constant.SHOE_SIZE_TABLE)
	updated := true
	for _, item := range body {
		//ssids[i] = item.ShoeSizeId
		if _, err := col.UpdateOne(ctx, bson.M{"_id": item.ShoeSizeId}, bson.M{"$inc": bson.M{"available": - item.Quantity}}); err != nil {
			updated = false
		}
	}
	//if res, err := col.UpdateMany(ctx, bson.M{"_id": bson.M{"$in": ssids}}); err != nil {
	//	
	//}
	return updated
}
